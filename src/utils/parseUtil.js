export const mergeData = (cardIndex, objectIndex) => {
  return [
    parseUtilComparison[objectIndex],
    parseUtilCards[cardIndex][objectIndex]
  ];
};
const parseUtilCards = [
  {
    0: "labor_cost_per_square_foot",
    1: "extended_labor_cost"
  },
  {
    0: "extended_screw_cost",
    // not sure where other value comes from...
    1: "extended_screw_cost"
  },
  {
    0: "spot_fastener_labor_cost_per_square_foot",
    1: "spot_fastener_extended_labor_cost"
  },
  {
    0: "spot_fastener_material_cost_per_square_foot",
    1: "spot_fastener_extended_material_cost"
  }
];

const parseUtilComparison = ["national_gypsum", "competitor"];
