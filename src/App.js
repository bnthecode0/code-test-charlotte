import React from "react";
import { withStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";
import Main from "./components/main/Main";
import Header from "./components/persistent/Header";

const styles = () => ({
  appWrapper: {
    position: "absolute",
    height: "100vh",
    width: "100vw",
    right: 0,
    top: 0,
    left: 0,
    backgroundColor: "#eee"
  }
});

function App(props) {
  const { classes } = props;
  return (
    <Grid className={classes.appWrapper} container>
      <Header />
      <Main />
    </Grid>
  );
}

export default withStyles(styles)(App);
