import React from "react";
import { withStyles } from "@material-ui/styles";
import ngLogo from "../../images/national-gypsum-logo.png";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const styles = () => ({
  logo: {
    height: "18%",
    maxHeight: 75
  },
  appBar: {
    backgroundColor: "white"
  }
});

function Header(props) {
  const { classes } = props;
  return (
    <AppBar className={classes.appBar}>
      <Toolbar>
        <img alt="ng" className={classes.logo} src={ngLogo} />
        <Typography variant="h6" className={classes.title}>
          News
        </Typography>
        <Button color="inherit">Login</Button>
      </Toolbar>
    </AppBar>
  );
}

export default withStyles(styles)(Header);
