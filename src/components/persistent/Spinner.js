import React from "react";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

const styles = theme => ({
  root: {
    height: "100vh",
    width: "100vw",
    backgroundColor: "#eee"
  },
  spinner: {
    marginTop: "20vh",
    marginLeft: "50vw"
  }
});

const Spinner = props => {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <CircularProgress className={classes.spinner} size={100} />
    </div>
  );
};
export default withStyles(styles)(Spinner);
