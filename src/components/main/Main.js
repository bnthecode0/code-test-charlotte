import React from "react";
import { withStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";
import { connect } from "react-redux";
import { postToCalculations } from "../../redux/actions/calculationActions";
import CalculationTab from "./calculation-tab/CalculationTab";
import ComparisonTab from "./comparison-tab/ComparisonTab";
import Spinner from "../persistent/Spinner";

const styles = () => ({
  mainWrapper: {
    backgroundColor: "#eee"
  }
});
const renderMain = ({ postToCalc, calculationsData }, loading) =>
  loading ? (
    <Spinner />
  ) : (
    <React.Fragment>
      <ComparisonTab calculationsData={calculationsData} />
    </React.Fragment>
  );

function Main(props) {
  const { classes, postToCalc, loading } = props;

  return (
    <Grid item md={12} className={classes.mainWrapper}>
      <CalculationTab postToCalc={postToCalc} />
      {renderMain(props, loading)}
    </Grid>
  );
}
const mapDispatchToProps = dispatch => ({
  postToCalc: payload => dispatch(postToCalculations(payload))
});
const mapStateToProps = state => ({
  calculationsData: state.calculation.calculationData,
  loading: state.ui.notifications.spinner
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Main));
