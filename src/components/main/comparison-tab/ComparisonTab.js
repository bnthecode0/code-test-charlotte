import React from "react";
import { withStyles } from "@material-ui/styles";
import {
  Grid,
  Typography,
  Card,
  CardHeader,
  CardContent
} from "@material-ui/core";
import { isEmpty } from "lodash";
import { mergeData } from "../../../utils/parseUtil";
import currencyFormatter from "currency-formatter";
import styles from "./comparison-tab-styles";

const laborTitle = "Overal labor savings (% / cost)";
const materialTitle = "Overal Material savings (% / cost)";
const cardTypes = [
  {
    title: "Labor",
    subtitle: laborTitle
  },
  {
    title: "Screws",
    subtitle: materialTitle
  },
  {
    title: "Spot Fasteners Labor",
    subtitle: laborTitle
  },
  {
    title: "Spot Fasteners Material",
    subtitle: materialTitle
  }
];
const formatToDollar = value =>
  currencyFormatter.format(value, { code: "USD" });
const getValueByIndex = (cardIndex, objIndex, obj) => {
  const [prop1, prop2] = mergeData(cardIndex, objIndex);
  return obj[prop1][prop2];
};
const getGridSquares = (numSquares, cardIndex, objIndex, className, obj) =>
  Array(numSquares)
    .fill()
    .map((value = null, index) => (
      <Grid className={className} item xs={5}>
        {formatToDollar(getValueByIndex(cardIndex, objIndex, obj))}
      </Grid>
    ));
const calcSavingsReducer = (acc, value) => acc + value;
const getAllCosts = arr => {
  return arr.map(data => Object.values(data).reduce(calcSavingsReducer));
};
const getCostSavings = calcType => {
  // this is actually in the data, no need for this
  const { competitor, national_gypsum } = calcType;
  const [competitorCosts, ngCosts] = getAllCosts([competitor, national_gypsum]);
  return ngCosts - competitorCosts;
};
const getPercentageSavings = calcType => {
  const { competitor, national_gypsum } = calcType;
  const [competitorCosts, ngCosts] = getAllCosts([competitor, national_gypsum]);
  return (ngCosts / competitorCosts) * 100 - 100 + "%";
};

function ComparisonTab(props) {
  const { classes, calculationsData } = props;
  const horizontal = calculationsData["horizontal"];
  const vertical = calculationsData["vertical"];
  const comparisons = [vertical, horizontal];
  return !isEmpty(calculationsData)
    ? comparisons.map((calculationType, index) => (
        <Card
          elevation={12}
          className={classes.paperWrapper}
          style={{ borderTop: `8px solid ${index ? "#ffcc00" : "#cc0000"}` }}
        >
          <CardHeader
            classes={{ title: classes.cardTitle }}
            className={classes.cardHeader}
            title={`Comparison #${index +
              1}: GridMarX (12/12) vs Leading Competitor (8/12)`}
          ></CardHeader>
          <CardContent className={classes.cardContent}>
            <Grid container className={classes.cardSubheader}>
              <Grid item xs={12}>
                <Typography variant="subtitle2">
                  {`Overal Material and Labor Savings ${
                    index ? "Horizontal" : "Vertical"
                  }`}
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography align="center" variant="body2">
                  Cost Savings
                </Typography>
                <Typography gutterBottom align="center" variant="h6">
                  {formatToDollar(getCostSavings(calculationType, index))}
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography align="center" variant="body2">
                  % Savings
                </Typography>
                <Typography gutterBottom align="center" variant="h6">
                  {getPercentageSavings(calculationType, index)}
                </Typography>
              </Grid>
            </Grid>
            <Grid container className={classes.cardWrappers} spacing={4}>
              {cardTypes.map((card, cardIndex) => (
                <Grid className={classes.compareCard} item xs={6}>
                  <Card>
                    <CardHeader
                      classes={{
                        title: index
                          ? classes.cardSubHeaderHorizontal
                          : classes.cardSubHeaderVertical
                      }}
                      className={
                        index
                          ? classes.subCardHeaderHorizontal
                          : classes.subCardHeaderVertical
                      }
                      title={card.title}
                    />
                    <CardContent className={classes.innerCard}>
                      <Grid container spacing={0}>
                        {getGridSquares(
                          4,
                          cardIndex,
                          index,
                          classes.gridSquare,
                          calculationType
                        )}
                      </Grid>
                    </CardContent>
                    <Grid container className={classes.cardSubheader}>
                      <Grid item xs={12}>
                        <Typography variant="overline">
                          {card.subtitle}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </CardContent>
        </Card>
      ))
    : "";
}

export default withStyles(styles)(ComparisonTab);
