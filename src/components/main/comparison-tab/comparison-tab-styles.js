const styles = () => ({
  // going to break out repetitive classes to use classnames
  paperWrapper: {
    margin: "0px 0px 25px 25px ",
    backgroundColor: "white",
    width: "95%",
    fontFamily: "sans-serif"
  },
  cardHeader: {
    height: 60,
    padding: "0px 0px 0px 80px",
    backgroundColor: "#e0e0e0"
  },
  cardTitle: {
    fontSize: 14
  },
  cardContent: {
    padding: 0
  },
  cardSubheader: {
    padding: "20px 0px 0px 80px",
    height: 100,
    backgroundColor: "#eeeeee",
    width: "100%"
  },
  cardSubHeaderHorizontal: {
    fontSize: 16,
    color: "black"
  },
  subCardHeaderHorizontal: {
    backgroundColor: "#ffcc00",
    color: "black",
    height: 8
  },
  cardSubHeaderVertical: {
    fontSize: 16,
    color: "white"
  },
  subCardHeaderVertical: {
    backgroundColor: "#cc0000",
    color: "white",
    height: 8
  },
  gridSquare: {
    border: "1px solid black",
    height: 140,
    textAlign: "center",
    lineHeight: 6,
    fontSize: 24,
    justifyContent: "center"
  },
  cardWrappers: {
    padding: 16
  },
  innerCard: {
    paddingLeft: 200
  }
});

export default styles;
