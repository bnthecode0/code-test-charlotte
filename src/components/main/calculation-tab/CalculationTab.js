import React from "react";
import { withStyles } from "@material-ui/styles";
import {
  Grid,
  Paper,
  Typography,
  Divider,
  Button,
  TextField
} from "@material-ui/core";
import Slider from "../../inputs/Slider";

const styles = () => ({
  paperWrapper: {
    margin: "120px 0px 25px 25px ",
    backgroundColor: "white",
    height: 80,
    width: "95%",
    padding: 20,
    fontFamily: "sans-serif"
  },
  divider: {
    width: 4,
    height: "80px"
  },
  button: {
    width: "100%",
    fontWeight: 800,
    padding: 15,
    marginBottom: 12
  },
  textField: {
    borderRadius: 90,
    height: 15,
    [`& fieldset`]: {
      borderRadius: 25
    }
  },
  sliderWrapper: {
    paddingTop: 16
  },
  buttonWrapper: {
    textAlign: "center",
    color: "#9e9e9e"
  }
});

function CalculationTab(props) {
  const [calculationValues, setCalcValues] = React.useState({
    laborType: "union",
    zip: "",
    squareFootage: ""
  });
  const { laborType, zip, squareFootage } = calculationValues;

  const handleValueChange = (event, name) => {
    setCalcValues({
      ...calculationValues,
      [name]: event.target ? event.target.value : event
    });
  };
  const handleCalculate = () => {
    const { postToCalc } = props;
    if (laborType && zip && squareFootage) {
      postToCalc(calculationValues);
    }
  };

  const { classes } = props;
  return (
    <Paper elevation={12} className={classes.paperWrapper}>
      <Grid container>
        <Grid item xs={3} sm={3} md={3}>
          <Typography variant="caption">Labor Type:</Typography>
          <Grid className={classes.sliderWrapper} item xs={12}>
            <Slider handleValueChange={handleValueChange} />
          </Grid>
        </Grid>
        <Grid item xs={2}>
          <Typography variant="caption">Zip Code (First 3 digits):</Typography>
          <Grid item xs={12}>
            <TextField
              onChange={e => handleValueChange(e, "zip")}
              InputProps={{
                style: {
                  height: 40
                }
              }}
              id="outlined-basic"
              className={classes.textField}
              margin="normal"
              variant="outlined"
            />
          </Grid>
        </Grid>
        <Grid item xs={3}>
          <Typography variant="caption">Square Footage:</Typography>
          <Grid item xs={12}>
            <TextField
              onChange={e => handleValueChange(e, "squareFootage")}
              InputProps={{
                style: {
                  height: 40
                }
              }}
              id="outlined-basic"
              className={classes.textField}
              margin="normal"
              variant="outlined"
            />
          </Grid>
        </Grid>
        <Grid item xs={1}>
          <Divider className={classes.divider} />
        </Grid>
        <Grid item xs={3}>
          <Grid className={classes.buttonWrapper} item xs={12}>
            <Button
              onClick={() => handleCalculate()}
              className={classes.button}
              variant="contained"
              color="primary"
            >
              Calculate
            </Button>
            <Typography variant="caption">
              Location: National Average
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  );
}

export default withStyles(styles)(CalculationTab);
