import React from "react";
import { withStyles } from "@material-ui/styles";

const styles = () => ({
  rect: {
    cursor: "pointer"
  },
  smallRect: {
    cursor: "pointer",
    transform: "translateX(0)",
    transition: "all .3s"
  },
  text: {
    fontWeight: 800,
    margin: "auto",
    cursor: "pointer"
  }
});

function Slider(props) {
  const [sliderPostion, setSliderPostion] = React.useState("left");
  const { classes, handleValueChange } = props;
  const left = sliderPostion === "left";
  const setSlider = pos => {
    const changeDirection = pos === sliderPostion;
    handleValueChange(pos === "left" ? "union" : "non-union", "laborType");
    return !changeDirection ? setSliderPostion(left ? "right" : "left") : "";
  };

  return (
    <svg>
      <rect
        className={classes.rect}
        width="240"
        height="40"
        rx="22"
        fill="white"
        stroke="#eee"
        strokeWidth={2}
      />
      <rect
        className={classes.smallRect}
        width="120"
        height="40"
        rx="22"
        fill="#3f51b5"
        stroke="#3f51b5"
        x={left ? 0 : 120}
      />
      <text
        onClick={() => {
          setSlider("left");
        }}
        className={classes.text}
        x="42"
        y="25"
        font-family="Verdana"
        font-size="12"
        fill={left ? "white" : "black"}
      >
        Union
      </text>
      <text
        onClick={() => {
          setSlider("right");
        }}
        className={classes.text}
        x="142"
        y="25"
        font-family="Verdana"
        font-size="12"
        fill={left ? "black" : "white"}
      >
        Non-Union
      </text>
    </svg>
  );
}

export default withStyles(styles)(Slider);
