import axios from "axios";

const httpHelper = {
  postToCalculations: async calcPayload =>
    await axios.post(
      "https://technekes.mockable.io/ngc-calculator-api/calculations",
      calcPayload
    )
};

export default httpHelper;
