import { createStore, applyMiddleware } from "redux";
import rootReducer from "../reducers";
import thunk from "redux-thunk";
import reduxMulti from "redux-multi";

const store = createStore(rootReducer, applyMiddleware(thunk, reduxMulti));

export default store;
