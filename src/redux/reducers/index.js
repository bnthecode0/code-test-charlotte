import { combineReducers } from "redux";
import uiReducer from "./uiReducer";
import calculationReducer from "./calculationReducer";

export default combineReducers({
  ui: uiReducer,
  calculation: calculationReducer
});
