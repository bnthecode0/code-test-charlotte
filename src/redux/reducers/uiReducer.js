const initialState = {
  notifications: {}
};
const uiReducer = (state = initialState, action) => {
  switch (action.type) {
    case "DISPLAY_SPINNER": {
      return {
        ...state,
        notifications: {
          ...state.notifications,
          spinner: action.payload
        }
      };
    }
    default:
      return state;
  }
};
export default uiReducer;
