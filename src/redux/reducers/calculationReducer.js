const initialState = {
  calculationData: {}
};

const calculationReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET_CALCULATION_DATA": {
      return {
        ...state,
        calculationData: action.payload
      };
    }
    default:
      return state;
  }
};
export default calculationReducer;
