import httpHelper from "../../http";
import displaySpinner from "./uiActions";

export const postToCalculations = payload => async dispatch => {
  try {
    dispatch(displaySpinner(true));
    const { laborType, zip, squareFootage } = payload;
    const calcPayload = {
      calculator_type: "gold_bond",
      union: laborType === "union" ? true : false,
      location: zip,
      square_footage: squareFootage
    };
    const {
      data: { GoldBondCalculation: calcData }
    } = await httpHelper.postToCalculations(calcPayload);
    dispatch(setCalculationData(calcData));
    dispatch(displaySpinner(false));
  } catch (error) {
    // set generic notification something went wrong
  }
};

const setCalculationData = calcData => ({
  type: "SET_CALCULATION_DATA",
  payload: calcData
});
