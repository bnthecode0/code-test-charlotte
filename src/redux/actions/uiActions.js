const displaySpinner = bool => ({
  type: "DISPLAY_SPINNER",
  payload: bool
});

export default displaySpinner;
