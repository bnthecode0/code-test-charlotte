## `------For Reviewers-----`

There were issues completely reading the data as it was same across horizontal / vertical. Fields seemed to be missing also. Spent about 5 hours total to get this up and running. Please be aware it is not meant to be finished, while I am very aware that there are issues with small styling, data, and others. This is meant to be a display of knowledge, and patterns set in place to show reusablity, and seperation. 

To start application ---> /code-test-charlotte --> npm install --> npm start




In the project directory, you can run:
## Scripts
### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000] to view in browser

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run lint:format`

Runs prettier on all src files

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!




